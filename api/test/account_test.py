from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountQueries


client = TestClient(app)


class FakeAccountQueries:
    fake_db = {"accounts": []}

    def get_all_accounts(self):
        try:
            return self.fake_db["accounts"]
        except Exception as e:
            return {"message": str(e)}


class FakeCreateAccount:
    def create_account(self, data, hashed_password):
        try:
            account = {
                "id": len(self.fake_db["accounts"])+1,
                "username": data.username,
                "email": data.email,
                "password": hashed_password
            }
            self.fake_db["accounts"].append(account)
            return account
        except Exception as e:
            return {"message": str(e)}


def test_get_accounts():
    app.dependency_overrides[AccountQueries] = FakeAccountQueries
    # dummy_data = {
    #     "username": "testymctesty",
    #     "email": "testemail@email.com",
    #     "password": "test123",
    # }

    response = client.get("/api/accounts/")
    assert response.status_code == 200

    app.dependency_overrides[AccountQueries] = AccountQueries()


def test_create_acc():
    app.dependency_overrides[AccountQueries] = FakeCreateAccount
    test_data = {
        "username": "testymctesty",
        "email": "testemail@email.com",
        "password": "test123",
    }
    response = client.post("/api/accounts", json=test_data)

    assert response.status_code == 200
    assert response.json()["username"] == test_data["username"]
    assert response.json()["email"] == test_data["email"]
    assert response.json()["password"] == test_data["password"]

    app.dependency_overrides[AccountQueries] = AccountQueries()
