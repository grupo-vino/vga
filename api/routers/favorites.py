from fastapi import Depends, APIRouter, HTTPException
from authenticator import authenticator
from typing import Optional
from queries.favorites import (
    FavoriteIn,
    FavoriteQueries
)


router = APIRouter()


@router.post("/favorite")
def create_favorite(
    info: FavoriteIn,
    account: Optional[dict] = Depends(
        authenticator.try_get_current_account_data),
    repo: FavoriteQueries = Depends(),
):
    if account:
        return repo.create(info.game_id, account["id"])
    else:
        raise HTTPException(status_code=401, detail="Unauthorized")


@router.delete("/favorite", response_model=bool)
def delete_favorite(
    info: FavoriteIn,
    account: Optional[dict] = Depends(
        authenticator.try_get_current_account_data),
    repo: FavoriteQueries = Depends(),
) -> bool:
    if account:
        return repo.delete(info.game_id, account["id"])
    else:
        raise HTTPException(status_code=401, detail="Unauthorized")


@router.get("/favorite")
async def get_favorites(
    repo: FavoriteQueries = Depends(),
    account: Optional[dict] = Depends(
        authenticator.try_get_current_account_data),
):
    if account:
        return repo.get_all(account["id"])
    else:
        raise HTTPException(status_code=401, detail="Unauthorized")
