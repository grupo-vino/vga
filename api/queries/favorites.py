from typing import Union, List
from pydantic import BaseModel
from .pool import pool


class Error(BaseModel):
    message: str


class DuplicateError(ValueError):
    pass


class FavoriteIn(BaseModel):
    game_id: int


class FavoriteOut(BaseModel):
    id: int


class FavoriteQueries:
    def create(self, game_id, user_id: int) -> Union[bool, Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(
                        """
                        INSERT INTO favorites (game_id, user_id)
                        VALUES (%s, %s)
                        """,
                        [game_id, user_id],
                    )
                    return True
                except Exception as e:
                    return {"message": str(e)}

    def delete(self, info, user_id: int) -> Union[bool, Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(
                        """
                        DELETE FROM favorites
                        WHERE game_id = %s AND user_id = %s
                        """,
                        [info, user_id],
                    )
                    return True
                except Exception as e:
                    return {"message": str(e)}

    def get_all(self, user_id) -> List[int]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(
                        """
                        SELECT game_id
                        FROM favorites
                        WHERE user_id = %s
                        """,
                        [user_id],
                    )
                    record = []
                    for row in cur.fetchall():
                        if row[0] not in record:
                            record.append(row[0])
                    return record
                except Exception as e:
                    return {"message": str(e)}
