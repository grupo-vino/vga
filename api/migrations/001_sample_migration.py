steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE singlepage (
            id SERIAL PRIMARY KEY NOT NULL UNIQUE,
            title VARCHAR(100) NOT NULL,
            short_description TEXT,
            game_url VARCHAR(250) NOT NULL UNIQUE,
            thumbnail VARCHAR(100) NOT NULL UNIQUE,
            genre VARCHAR(100) NOT NULL,
            platform VARCHAR(100) NOT NULL,
            publisher VARCHAR(100) NOT NULL,
            developer VARCHAR(100) NOT NULL,
            release_date DATE NOT NULL
    );
        """,
        # "Down" SQL statement
        """
        DROP TABLE singlepage;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE,
            username VARCHAR(100) NOT NULL UNIQUE,
            hashed_password VARCHAR(100) NOT NULL
    );
        """,
        # "Down" SQL statement
        """
        DROP TABLE accounts;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE favorites (
            id SERIAL PRIMARY KEY,
            game_id INT NOT NULL,
            user_id SERIAL NOT NULL,
            CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES accounts(id)
    );
        """,
        # "Down" SQL statement
        """
        DROP TABLE favorites;
        """,
    ],
]
