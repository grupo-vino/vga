from pydantic import BaseModel


class GameItem(BaseModel):
    id: int
    title: str
    thumnail: str
    short_description: str
    genre: str
    platform: str
    publisher: str
    developer: str
    release_date: str
