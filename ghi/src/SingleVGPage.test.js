import * as React from "react";
import { render, screen } from "@testing-library/react";
import VideogameInfo from "./SingleVGPage.js";
import { GamesContext, AuthContext } from "./contexts";
import renderer from "react-test-renderer";

const games = {
  540: {
    id: 540,
    title: "Overwatch 2",
    thumbnail: "https://www.freetogame.com/g/540/thumbnail.jpg",
    short_description:
      "A hero-focused first-person team shooter from Blizzard Entertainment.",
    game_url: "https://www.freetogame.com/open/overwatch-2",
    genre: "Shooter",
    platform: "PC (Windows)",
    publisher: "Activision Blizzard",
    developer: "Blizzard Entertainment",
    release_date: "2022-10-04",
    freetogame_profile_url: "https://www.freetogame.com/overwatch-2",
  },
};
const auth = null;
const setGames = jest.fn((data) => (games = data));
const setAuth = jest.fn((data) => (auth = data));

describe("SingleVGPage", () => {
  test("should render correctly", () => {
    const tree = renderer
      .create(
        <AuthContext.Provider value={{ auth, setAuth }}>
          <GamesContext.Provider value={{ games, setGames }}>
            <VideogameInfo />
          </GamesContext.Provider>
        </AuthContext.Provider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  test("should render the game image", async () => {
    render(
      <AuthContext.Provider value={{ auth, setAuth }}>
        <GamesContext.Provider value={{ games, setGames }}>
          <VideogameInfo />
        </GamesContext.Provider>
      </AuthContext.Provider>
    );

    const gameImage = await screen.findByAltText(/game image/i);

    expect(gameImage).toBeInTheDocument();
  });
});
