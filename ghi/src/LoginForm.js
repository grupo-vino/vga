import React, { useState, useContext, useEffect } from "react";
import { AuthContext } from "./contexts";
import { Navigate } from "react-router-dom";
import styles from "./LoginForm.module.css";

function LoginForm() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { auth, setAuth } = useContext(AuthContext);

  const handleLogin = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/token`;
    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    const fetchConfig = {
      method: "post",
      body: new URLSearchParams(formData),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);

      if (response.ok) {
        const responseJson = await response.json();

        setAuth(responseJson.access_token);
        localStorage.setItem("auth", responseJson.access_token);
        setUsername("");
        setPassword("");
        Navigate("/");
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className={styles.container}>
      <h2 className={styles.header}>Login</h2>
      <div className={styles.form}>
        <div className={styles.inputcontainer}>
          <input
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            data-testid="username-input"
            className={styles.input}
          />
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            data-testid="password-input"
            className={styles.input}
          />
        </div>
        <button
          onClick={handleLogin}
          data-testid="login-button"
          className={styles.button}
        >
          Log In
        </button>
      </div>
    </div>
  );
}

export default LoginForm;
