import React, { useState, useEffect } from "react";

function GameList() {
  const [games, setGames] = useState([]);
  const fetchData = async () => {
    const url =
      "https://corsproxy.io/?https%3A%2F%2Fwww.freetogame.com%2Fapi%2Fgames";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setGames(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="mt-4">
        <h1 className="game-header">Free Games</h1>
      </div>
      <div className="game-container">
        {games?.map((game) => {
          return (
            <>
            <div className="game-card">
              <div key={game.id}></div>
              <img className="game-img" src={game.thumbnail} alt="issa game"></img>
              <div className="text-card">
              <h2>{game.title}</h2>
              <h3>{game.genre}</h3>
              <h4>{game.short_description}</h4>
              <a className="url-card" href={game.game_url}>{game.game_url}</a>
              </div>
              </div>
            </>
          );
        })}

      </div>
    </div>
  );
}

export default GameList;
