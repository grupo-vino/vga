import React, { useEffect, useState, useMemo, useContext } from "react";
import { useParams } from "react-router-dom";
import { AuthContext, GamesContext } from "./contexts";
import styles from "./SingleVGPage.module.css";
import { ReactComponent as HeartLiked } from "./heart-solid.svg";
import { ReactComponent as HeartNotLiked } from "./heart-regular.svg";

function LikeButton() {
  const [like, setLike] = useState(false);
  const { id } = useParams();
  const { auth } = useContext(AuthContext);
  const bearerToken = useMemo(() => `Bearer ${auth}`, [auth]);

  useEffect(() => {
    const getFavorites = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_HOST}/favorite`,
          {
            method: "GET",
            headers: {
              Authorization: bearerToken,
            },
          }
        );
        const jsonResponse = await response.json();

        if (jsonResponse.includes(Number(id))) {
          setLike(true);
        }
      } catch (error) {
        console.error(error);
      }
    };

    if (auth !== null) {
      getFavorites();
    }
  }, [auth]);

  const handleLike = () => {
    try {
      fetch(`${process.env.REACT_APP_API_HOST}/favorite`, {
        method: "POST",
        body: JSON.stringify({ game_id: Number(id) }),
        headers: {
          Authorization: bearerToken,
          "Content-Type": "application/json",
        },
      });
    } catch (error) {
      console.error(error);
    }

    setLike(true);
  };

  const handleUnlike = () => {
    try {
      fetch(`${process.env.REACT_APP_API_HOST}/favorite`, {
        method: "DELETE",
        body: JSON.stringify({ game_id: Number(id) }),
        headers: {
          Authorization: bearerToken,
          "Content-Type": "application/json",
        },
      });
    } catch (error) {
      console.error(error);
    }

    setLike(false);
  };

  return (
    <div>
      {like === true ? (
        <HeartLiked onClick={handleUnlike} className={styles.likeButton} />
      ) : (
        <HeartNotLiked onClick={handleLike} className={styles.likeButton} />
      )}
    </div>
  );
}

function VideogameInfo() {
  const { id } = useParams();
  const { auth } = useContext(AuthContext);
  const { games } = useContext(GamesContext);
  const isLoggedIn = useMemo(() => auth !== null, [auth]);
  const game = useMemo(() => games[Number(id)] ?? {}, [games]);

  return (
    <div className={styles.container}>
      <div id="game-title" className={styles.titleInfo}>
        <span className={styles.title}>
          {game.title} ({game?.release_date?.split("-").at(0)})
        </span>
        {isLoggedIn && <LikeButton />}
      </div>
      <div className={styles.bubbleContainer}>
        <span className={styles.bubble}>{game.genre}</span>
        <span className={styles.bubble}>{game.platform}</span>
        <span className={styles.bubble}>{game.publisher}</span>
        {game.publisher !== game.developer ? (
          <span className={styles.bubble}>{game.developer}</span>
        ) : null}
      </div>
      <div className={styles.gameContainer}>
        <img
          className={styles.gameImage}
          src={game.thumbnail}
          alt="game image"
        />
        <div className={styles.informationContainer}>
          <div>
            <p className={styles.releaseDate}>
              Release Date: {game.release_date}
            </p>
            <p className={styles.getGame}>
              Get Game&nbsp;
              <a href={game.game_url} target="_blank" className={styles.link}>
                Here!
              </a>
            </p>
          </div>
        </div>
      </div>
      <div>
        <div id="game-description" className={styles.description}>
          {game.short_description}
        </div>
      </div>
      <div id="makingHeartAnimationInProgress"></div>
    </div>
  );
}

export default VideogameInfo;
