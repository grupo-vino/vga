import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <div className="pos-f-t">
      <div className="collapse" id="navbarToggleExternalContent">
        <div className="bg-dark p-4">
          <div>
            <NavLink className="nav-link" to="/">
              VGA
            </NavLink>
          </div>
          <div>
            <NavLink className="nav-link" to="/jokelist">
              Jokes
            </NavLink>
          </div>
          <div>
            <NavLink className="nav-link" to="/videogames">
              Video Games
            </NavLink>
          </div>
          <div>
            <NavLink className="nav-link" to="/signup">
              Sign Up!
            </NavLink>
          </div>
          <div>
            <NavLink className="nav-link" to="/login">
              Login!
            </NavLink>
          </div>
          <div>
            <NavLink className="nav-link" to="/favorites">
              Favorites
            </NavLink>
          </div>
        </div>
      </div>
      <nav className="navbar navbar-light bg-dark">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarToggleExternalContent"
          aria-controls="navbarToggleExternalContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
      </nav>
    </div>
  );
}

export default Nav;
