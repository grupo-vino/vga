import React, { useEffect, useState, useMemo, useContext } from "react";
import { AuthContext, GamesContext } from "./contexts";
import styles from "./FavoritesPage.module.css";

function Favorites() {
  const { games } = useContext(GamesContext);
  const { auth } = useContext(AuthContext);
  const [gameFavsIDs, setGamefavsIDs] = useState([]);
  const bearerToken = useMemo(() => `Bearer ${auth}`, [auth]);
  const favoritedGames = useMemo(() => {
    return Object.values(games).filter((game) => gameFavsIDs.includes(game.id));
  }, [games, gameFavsIDs]);

  // if(!token) {
  //   return <Login />
  // }
  // Need login working to redirect in not logged in

  useEffect(() => {
    const getFavorites = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_HOST}/favorite`,
          {
            method: "GET",
            headers: {
              Authorization: bearerToken,
            },
          }
        );
        const jsonResponse = await response.json();
        setGamefavsIDs(jsonResponse);
        console.log("gameFav: ", gameFavsIDs);
      } catch (error) {
        console.error(error);
      }
    };

    if (auth !== null) {
      getFavorites();
    }
  }, [auth, games]);

  return (
    <div className={styles.container}>
      <div className={styles.title}>
        <h1 className={styles.header}>My Favorites</h1>
      </div>
      <div className={styles.gameGrid}>
        {favoritedGames.map((favoritedGame) => (
          <div className={styles.gameContainer}>
            <h2>{favoritedGame.title}</h2>
            <a href={`videogames/${favoritedGame.id}`} rel="noreferrer">
              <img
                src={favoritedGame.thumbnail}
                className={styles.gameImg}
                alt="thumbnail"
              ></img>
            </a>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Favorites;
