import { createContext } from "react";

export const GamesContext = createContext([]);
export const AuthContext = createContext(localStorage.getItem("auth") ?? null);
