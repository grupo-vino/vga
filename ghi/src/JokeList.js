import React, { useState, useEffect } from "react";

function JokeList() {
  const [jokes, setJokes] = useState([]);
  const fetchData = async () => {
    const url = "https://official-joke-api.appspot.com/jokes/ten";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setJokes(data);
      console.log(data)
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

return (
    <div>
        <div className="mt-4">
          <h1 className="joke-header">Jokes</h1>
        </div>
        <div className="joke-container">
          {jokes?.map((joke) => {
            return (
              <>
              <div className="joke-card">
                <div key={joke.id}></div>
                <div className="joke-text">
                <h2>{joke.setup}</h2>
                <h3>{joke.punchline}</h3>
                </div>
                </div>
              </>
            );
          })}
      </div>
    </div>
  );
}
export default JokeList;
