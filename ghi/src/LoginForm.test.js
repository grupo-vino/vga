import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect"; // To use Jest DOM matchers

import LoginForm from "./LoginForm";

describe("LoginForm Component", () => {
  it("renders login form properly", () => {
    render(<LoginForm />);

    expect(screen.getByTestId("username-input")).toBeInTheDocument();
    expect(screen.getByTestId("password-input")).toBeInTheDocument();
    expect(screen.getByTestId("login-button")).toBeInTheDocument();
  });

  it("allows users to fill in the form and login", async () => {
    render(<LoginForm />);
    const usernameInput = screen.getByTestId("username-input");
    const passwordInput = screen.getByTestId("password-input");
    const loginButton = screen.getByTestId("login-button");

    fireEvent.change(usernameInput, { target: { value: "testuser" } });
    fireEvent.change(passwordInput, { target: { value: "testpassword" } });

    fireEvent.click(loginButton);

    // Perform assertions based on the expected behavior after a successful login attempt
    // For instance, you might expect certain functions to be called or certain elements to change.

    await waitFor(() => {
      // Check if the function to set localStorage was called
      expect(localStorage.setItem).toHaveBeenCalledTimes(1);
      // Check if login has been successfully processed
      // Add assertions based on your expected behavior here
    });
  });
});
