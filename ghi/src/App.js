import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useEffect, useState } from "react";
import ErrorNotification from "./ErrorNotification";
import "./App.css";
import Nav from "./Nav";
import VideogameInfo from "./SingleVGPage.js";
import SignUpForm from "./SignUpPage";
import { GamesContext, AuthContext } from "./contexts";
import Mainpage from "./MainPage";
import JokeList from "./JokeList";
import GameList from "./gameList2";
import { Link } from "react-router-dom";
import LoginForm from "./LoginForm";
import Favorites from "./FavoritesPage";

function App() {
  const [, setLaunchInfo] = useState([]);
  const [error, setError] = useState(null);
  const [games, setGames] = useState({});
  const [auth, setAuth] = useState(localStorage.getItem("auth"));

  const getGameData = async () => {
    try {
      const res = await fetch(
        "https://corsproxy.io/?https%3A%2F%2Fwww.freetogame.com%2Fapi%2Fgames"
      );
      const resJson = await res.json();
      const gameData = {};
      for (const game of resJson) {
        gameData[game.id] = game;
      }
      setGames(gameData);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    async function getData() {
      let url = `${process.env.REACT_APP_API_HOST}/api/launch-details`;
      //console.log("fastapi url: ", url);
      let response = await fetch(url);
      //console.log("------- hello? -------");
      let data = await response.json();

      if (response.ok) {
        console.log("got launch data!");
        setLaunchInfo(data.launch_details);
      } else {
        console.log("drat! something happened");
        setError(data.message);
      }
    }
    getGameData();
    getData();
  }, []);

  return (
    <GamesContext.Provider value={{ games, setGames }}>
      <AuthContext.Provider value={{ auth, setAuth }}>
        <div>
          <ErrorNotification error={error} />
          <BrowserRouter>
            <Nav />
            <Routes>
              <Route path="/videogames">
                <Route path="" element={<GameList />} />
                <Route path=":id" element={<VideogameInfo />} />
              </Route>
              <Route path="/signup" element={<SignUpForm />} />
              <Route path="/" element={<Mainpage />} />
              <Route path="jokelist" element={<JokeList />} />
              <Route path="gamelist" element={<GameList />} />
              <Route path="/favorites" element={<Favorites />} />
              <Route path="/login" element={<LoginForm />} />
            </Routes>
          </BrowserRouter>
        </div>
      </AuthContext.Provider>
    </GamesContext.Provider>
  );
}

export default App;
