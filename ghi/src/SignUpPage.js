import React, { useState, useContext } from "react";
import { AuthContext } from "./contexts";
import { useNavigate } from "react-router-dom";
import styles from "./SignUpPage.module.css";
import "@lottiefiles/lottie-player";

function BootstrapInput({ id, placeholder, labelText, value, onChange, type }) {
  return (
    <div className={styles.inputContainer}>
      <label htmlFor={id} className={styles.label}>
        {labelText}
      </label>
      <input
        value={value}
        onChange={onChange}
        required
        type={type}
        className={styles.input}
        id={id}
        placeholder={placeholder}
      />
    </div>
  );
}

function SignUpForm() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const { setAuth } = useContext(AuthContext);

  const handleSubmit = async () => {
    if (username === "" || email === "" || password === "") return;

    const url = `${process.env.REACT_APP_API_HOST}/api/accounts`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify({
        username,
        email,
        password,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);

      if (response.ok) {
        const responseJson = await response.json();
        const { access_token } = responseJson;

        localStorage.setItem("auth", access_token);

        setAuth(access_token);
        navigate("/");
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.lottie}>
        <lottie-player
          src="https://lottie.host/5bca8660-e6d7-491f-b7a4-bd3655897be3/7OocjniJgL.json"
          background="transparent"
          speed="1"
          loop
          autoplay
        ></lottie-player>
      </div>
      <h1 className={styles.header}>Signup Page</h1>
      <form className={styles.form}>
        <BootstrapInput
          id="email"
          placeholder="you@example.com"
          labelText="Your email address"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type="text"
        />
        <BootstrapInput
          id="name"
          placeholder="Billy Bob Joe"
          labelText="Your username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          type="text"
        />
        <BootstrapInput
          id="password"
          placeholder="Your super duper secret password"
          labelText="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
        />
        <button type="button" className={styles.button} onClick={handleSubmit}>
          Submit
        </button>
      </form>
    </div>
  );
}

export default SignUpForm;
