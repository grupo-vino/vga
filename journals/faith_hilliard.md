## Faith's Journals

# Sep 18th - Sep 20th:

- Worked on the Excalidraw with the other devs in my group to create a simple mock design of the web app

# Sep 20th:

- I put together a google document to list necessary tasks needed to be completed for the project

# Sep 25th:

- The group decided to redo the project idea, so we worked on the new Excalidraw

# Sep 27th:

- My group discussed the rating of difficulty for each individual task and talked about what would be distributed to whom

# Sep 28th:

- I found several third party APIs that we could use and made a list

# Oct 3rd:

- Got my docker container and images up and running

# Oct 9th:

- While attempting to start up my docker container and images again, it wasn't working. Trying to find the issue

# Oct 11th:

- Docker is still not working, I moved on to writing the front end code for my Game List page

# Oct 13th:

- With Docker not running, I started using Node for my local server to test my page
- I finished the front end fetch function for the external API and confirmed that it is working

# Oct 14th:

- We had a group meeting at 4pm central to work on the project together. I ended up working alone on the third party API usage on the game page that I'm in charge of.

# Oct 16th:

- The single videogame page is mostly completed. Need to work on other things before completion is able to happen

# Oct 17th:

- I got external assistance to get my docker working and we found out that it wasn't working because of some issue with the windows web sockets. Once that was fixed, my Docker was working once again

# Oct 18th:

- I started the signup page. I worked on using auth backend api that Jack made.
- I created the git issues for the project

# Oct 20th:

- Signup page completed! Used contexts and made a function to handle the new user account information

# Oct 21st:

- I picked out a color set and got them approved by the rest of the group

# Oct 23rd:

- Tasks are reassigned for better project completion
- Worked with Joe to tell him how to use the auth and contexts for his sign in page
- Worked with Dante to tell him how to make the game jokes page
- Completed the game list page and the search bar stretch goal

# Oct 24th:

- Worked on creating a unit test with jest for the sign up page. Incomplete.

# Oct 25th:

- The unit test created tests the ability to sign up for an account on the web application
- I worked with Jack to get my merge requests reviewed and appropriately merged them onto the main branch

# Oct 26th:

- I reviewed merge requests
- Sign Up Page functionality was deemed complete
- Updated my journal

# Oct 27th:

- Single Videogame Page functionality complete
- Finished readme for project
- Helped Jack and Joe with merge requests
- Bug fix on the sign up page
- Helped with the API
- Updated global styles to match design
- Added styling to both the sign up page and the single videogame page
- Added the game link to the single videogame page

# Oct 28th:

- Updated more CSS to the sign up page
- Assisted with the API
- Assisted with the Sign In Page

# Oct 29th:

- Assisted with the API
- Worked with Jack on his merge request

# Oct 30th:

- Added a cursor pointer when hovering the favorite / heart on SingleVGPage
- Cleaned up dead code while looking through some code
- Updated my unit tests
