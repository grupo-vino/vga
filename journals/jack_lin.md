9/27/2023:
worked on Nav barebones, will add more content as more JS pages becomes available.

9/28/2023:
Continuing work on backend APIs

10/9/2023:
Working on setting up init file for database

10/11/2023:
Started working on backend auth

10/16/2023:
Finishing up backend APIs

10/18/2023
Continuing working on backend auth

10/19/2023:
found some bugs in backend API, endpoints not working as designed
Fixed some bugs, update and delete not working still

10/23/2023:
Group talked about removing some functions and designating them to be stretch goals

10/24/2023:
Worked on backend test

10/26/2023:
Meeting with seirs and instructors, looked at MVPs and tried to mediate between Dante and Faith

10/27/2023:
Finished up backend auth and began merge requests and looking at frontend

10/28:
Worked on debugging some backend api issues with Faith

10/29:
tried to get some of the not working frontend to work with faith. was unabled to.
