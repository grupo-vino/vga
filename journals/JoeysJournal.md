## Joey's Journals

# Sep 18th - Sep 20th:

- team worked on creating an excelidraw of what we want our website to look like.

# Sep 25th:

- The group decided to redo the project idea, so we worked on the new Excalidraw

# Sep 27th:

- Our group talked about the rating of difficulty for each individual task and talked about who would get what task

# Oct 8th:

- started to work on creating my docker containers

# Oct 9th:

- While attempting to start up my docker container and images again, it wasn't working. Trying to find the issue

# Oct 17th:

- I got help with creating my sign in models

# Oct 18th:

- I started the login page. I worked on using auth backend api that Jack made.

# Oct 24th:

- Login page completed!
