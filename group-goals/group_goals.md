## Group Goals
Use a date, quick description of what was done that day and next step goals.

#### 9/25/2023 Monday
A google document was created with a checklist of tasks required to complete this single page web application. A list of API's that may be of use to this project was added to the google document as well.
https://docs.google.com/document/d/1hjEKAUencsSle2_ubNUfvrntPDyPFvWkYOeM0y7GRio/edit?usp=sharing
<br>
Next day goal is to assign checklist tasks to individual group members.

#### 9/26/2023 Tuesday
A checklist was added to the group google document using the project rubric as a reference.
<br>
Next day goals
