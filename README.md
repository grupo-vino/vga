# Video Game Addicts (VGA)

<!-- TOC start (generated with https://github.com/derlin/bitdowntoc) -->

- [About The Project](#about-the-project)
  - [Functionality](#functionality)
  - [Use Cases](#use-cases)
  - [Intended Market](#intended-market)
  - [Future Functionality (Stretch Goals)](#future-functionality-stretch-goals)
- [Tech Stack](#tech-stack)
- [Onboarding](#onboarding)
  - [Install VSCode Extensions](#install-vscode-extensions)
  - [Setup Local Environment Variables](#setup-local-environment-variables)
  - [Starting Development Server](#starting-development-server)
  - [Testing](#testing)
    - [Current Tests](#current-tests)
  - [Branching Strategy](#branching-strategy)
- [Issues](#issues)

<!-- TOC end -->

## About The Project

This is an information web application that contains a directory of video games that are free to play. Users are also able to favorite games to keep a personalized list of the games that they like.

### Functionality

- Our RESTful API enables CRUD operations for user's to maintain a list of favorited games
- Users are able to create an account to maintain a list of favorited games

### Use Cases

- Scenario: Bob is looking for a free game to play tonight \
  When Bob views the video game directory \
  Then Bob clicks on a game that looks interesting \
  And Bob gets information about the game

- Scenario: Jack wants to have a list of games that he likes \
  Given Jack likes a game that he sees on the video game directory \
  When Jack saves the game to his "favorites" list \
  Then Jack views the list of all the games he favorited on the web application

### Intended Market

VGA is aimed at gamers who want to save money while fulfilling their gaming hobby.

### Future Functionality (Stretch Goals)

- Allow users to filter the list of video games via specific genres, developers, publishers, etc
- Allow users to filter the list of video games via a search bar
- Add a pretty background video to the home page
- Add a friendly toast notification when a user "likes" a game.

## Tech Stack

- [React](https://react.dev)
- [Python](https://python.org)
- [Postgresql](https://postgresql.org)
- [FastAPI](https://fastapi.tiangolo.com)

## Onboarding

### Install VSCode Extensions

- Prettier: <https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode>
- Black Formatter: <https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter>

### Setup Local Environment Variables

Copy `.env.sample` to `.env` and modify the values as needed.

### Starting Development Server

**BE SURE YOU HAVE YOUR `.env` FILE SET UP BEFORE DOING THIS**

The development server can be started by running the command `docker-compose up` in the command line. This will start a FastAPI container, Postgresql container, PGAdmin container, and a web container for the react application.

The frontend can be accessed via http://localhost:3000

The internal API can be accessed via http://localhost:8000

PGAdmin can be accessed via http://localhost:8082

### Testing

All unit tests for the API should be put in `api/test`.

All unit tests for the React frontend should be put in `ghi/src`.
The name of the test file should be `{COMPONENT_NAME}.test.js` and should use Jest.

#### Current Tests

- [api test_games.py](https://gitlab.com/grupo-vino/vga/-/blob/auth_branch/api/test/test_games.py)
- [frontend SingleVGPage.test.js](https://gitlab.com/grupo-vino/vga/-/blob/main/ghi/src/SingleVGPage.test.js?ref_type=heads)

### Branching Strategy

Branches should be made for each new feature. New code should **not** be committed directly to the `main` branch. All contributions should be merged into `main` via a merge request through [the gitlab repository](https://gitlab.com/grupo-vino/vga/-/merge_requests)

## Issues

All issues should be reported on [the gitlab repository](https://gitlab.com/grupo-vino/vga/-/issues)
Please follow the provided template when reporting issues.
